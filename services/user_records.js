        let records = new Array(8);
        var mysql = require('mysql');
        let latestGet = [];
        for(let i=1 ; i<9; i++) {
            records[i-1] = 
                {
                    id: i,
                    name: "firstname" + i,
                    company: "company" + i,
                    code: "coded" + i,
                    date: (new Date()).toDateString(),
                    city: "tbilisi" + i,
                    country: "Georgia" + i
                };
        }
        const getAllRecords = () => {
            //==========================
            var customRecords = [];
        var connection = mysql.createConnection({
          host     : 'localhost',
          user     : 'root',
          password : '123456789',
          database : 'nodejs'
        });
        connection.connect()
        connection.query('SELECT * from customer', (error, results, fields) => {
            //console.log('The solution is: ', results);
            customRecords = JSON.parse(JSON.stringify(results))
            latestGet = [...customRecords]
        });
        connection.end();
            //===========================
            return (new Promise((resolve, reject) => {
                setTimeout(() => {
                    console.log(customRecords);
                    resolve(customRecords)
                }, 500);
            }));
        }
        const updateRecord = (record) => {
            var connection = mysql.createConnection({
                host     : 'localhost',
                user     : 'root',
                password : '123456789',
                database : 'nodejs'
              });
              connection.connect();
              //INSERT INTO customer(id, name, country, company, city, code, date, address, email, phone) VALUES(${i}, 'name${i}', 'country${i}', 'company ${i}', 'city${i}', 'code${i}', 'date${i}', 'address${i}', 'email${i}', 'phone${i}');
              connection.query(`
              UPDATE customer
               SET 
               name = '${record?.name ?? 'unknown'}',
               country = '${record?.country ?? 'unknown'}',
               company = '${record?.company ?? 'unknown'}',
               city = '${record?.city ?? 'unknown'}',
               code = '${record?.code ?? 'unknown'}',
               date = '${record?.date ?? 'unknown'}',
               address = '${record?.address ?? 'unknown'}',
               email = '${record?.email ?? 'unknown'}',
               phone = '${record?.phone ?? 'unknown'}'
            WHERE id = ${record.id}
              `, (error, results, fields) => {
                  //console.log('The solution is: ', results);
                  console.log(error, 'error');
              });
              connection.end();
            return (new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve(null);
                }, 500);
            }));
        }
        const  deleteRecord = (record) => {
            //=============================
            var connection = mysql.createConnection({
                host     : 'localhost',
                user     : 'root',
                password : '123456789',
                database : 'nodejs'
              });
              connection.connect();
              connection.query(`
              DELETE FROM customer WHERE id = ${record?.id}
              `, (error, results, fields) => {
                  //console.log('The solution is: ', results);
                  console.log(error, 'error');
              });
              connection.end();
            //==============================
            return (new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve(null)
                }, 500);
            }));
        }
        const addRecord = (record) => {
            //==============================
            var connection = mysql.createConnection({
                host     : 'localhost',
                user     : 'root',
                password : '123456789',
                database : 'nodejs'
              });
              connection.connect();
              //INSERT INTO customer(id, name, country, company, city, code, date, address, email, phone) VALUES(${i}, 'name${i}', 'country${i}', 'company ${i}', 'city${i}', 'code${i}', 'date${i}', 'address${i}', 'email${i}', 'phone${i}');
              connection.query(`
              INSERT INTO customer(id, name, country, company, city, code, date, address, email, phone)
              VALUES(${generatedId()}, '${record.name}', '${record.country}', '${record.company}', '${record.city}', '${record.code}', '${record.date}', '${record.address}', '${record.email}', '${record.phone}');
              `, (error, results, fields) => {
                  //console.log('The solution is: ', results);
                  console.log(error, 'error');
              });
              connection.end();
            //================================
            return (new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve(null)
                }, 500);
            }));
        }
        function getRecordById(id) {
            return records.find((record) => {
                return record.id == id;
            });
        }
        const allFunctions = {
            getAllRecords,
            updateRecord,
            updateRecord,
            deleteRecord,
            addRecord,
            getRecordById
        }
        function generatedId() {
            let cur = 0;
            for(i of latestGet) {
                cur = Math.max(i.id, cur);
            }
            return (cur + 1);
        }
        module.exports = allFunctions;