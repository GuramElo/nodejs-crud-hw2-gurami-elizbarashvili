var express = require('express');
var router = express.Router();

let {
    getAllRecords,
    updateRecord,
    deleteRecord,
    addRecord,
    getRecordById
} = require('../services/user_records');

router.get('/', async (req, res) => {
    res.render('crud', {Records: await getAllRecords()})
});
router.delete('/delete', async (req, res) => {
    await deleteRecord(req.body);
    res.send({
        status: "ok",
        statusCode: 200
    });
});
router.put('/put', async (req, res) => {
    await updateRecord(req.body);
    res.send({
        status: "ok",
        statusCode: 200
    });
});
router.post('/post', async (req, res) => {
    await addRecord(req.body);
    res.send({
        status: "ok",
        statusCode: 200
    });
});
router.get('/:id', (req, res) => {
    res.render('search_by_id', {Record: getRecordById(req.params.id)})
});

module.exports = router;